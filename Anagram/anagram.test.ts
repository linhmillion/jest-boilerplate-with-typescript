import { anagrams } from './anagram';
it("return all the words have same character and length", () =>{
  expect (anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada'])).toBe( ['aabb',
  'bbaa'])
})

it("return all the words have same character and length", () => {
  expect(anagrams('racer', ['crazer','carer', 'racar', 'caers', 'racer'])).toBe(['carer', 'racer'])
})

it("return all the words have same character and length", () =>{
  expect (anagrams('laser', ['lazing', 'lazy',  'lacer'])).toBe([])
})

// it(|"return all the words have same character and length", () =>{
//   expect (a)toBe()
// })
