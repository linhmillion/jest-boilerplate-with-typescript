import {happyAlgorithm} from './happyAlgorithm';

it("should return  a string 'happy +num' if number is 1", () => {
  expect(happyAlgorithm(139)).toBe("HAPPY 5")
})


it("should return  a string 'happy +num' if number is 1", () => {
  expect(happyAlgorithm(67)).toBe("SAD 10")
})
it("should return  a string 'happy +num' if number is 1", () => {
  expect(happyAlgorithm(1)).toBe("HAPPY 1")
})
it("should return  a string 'happy +num' if number is 1", () => {
  expect(happyAlgorithm(8)).toBe("SAD 8")
})
it("take only positive number as argument", () => {
  expect(happyAlgorithm(-10)).toBe("need to be a positive number")
})
