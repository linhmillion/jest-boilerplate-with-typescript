  export function  rearrange(string: string){
  const findNumber = (s = '') =>
  s.split('').reduce((acc, val) => (+val ? +val : acc), 0);

  const sorter = (a, b) => {
    return findNumber(a) - findNumber(b);
  };

  const arr = string.split(' ');
  arr.sort(sorter);
  // push the sorted words in a string
  let newStr = arr.join(' ');
  //remove all the number in the new string
  return newStr.replace(/[0-9]/g, '');
}
