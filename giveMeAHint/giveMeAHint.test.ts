import {grantTheHint} from './giveMeAHint'

it("should return an array with 5 string in it", () => {
  const text = grantTheHint("Mary Queen of Scots")
  const  result = ["____ _____ __ _____",
  "M___ Q____ o_ S____",
  "Ma__ Qu___ of Sc___",
  "Mar_ Que__ of Sco__",
  "Mary Quee_ of Scot_"]
  expect(text).toBe(result);
} )


it("should return an array with 5 string in it", () => {
  const text = grantTheHint("The Life of Pi")
  const  result = ["___ ____ __ __",
  "T__ L___ o_ P_",
  "Th_ Li__ of Pi",
  "The Lif_ of Pi",
  "The Life of Pi"]
  expect(text).toBe(result);
} )
