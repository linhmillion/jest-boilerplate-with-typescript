import {shuffleCount} from './shuffleCount'

it("return 3times shuffle if there is 8 cards i the deck",() =>{
  expect(shuffleCount(8)).toBe(3)
})

it("return 12times shuffle if there is 14 cards i the deck",() =>{
  expect(shuffleCount(14)).toBe(12)
})

it("return 8times shuffle if there is 52 cards i the deck",() =>{
  expect(shuffleCount(52)).toBe(8)
})

it("return false if there is minder than 2 cards i the deck",() =>{
  expect(shuffleCount(1)).toBe(false)
})
