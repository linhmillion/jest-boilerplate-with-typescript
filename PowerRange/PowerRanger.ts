

export function PowerRanger (n: number, a: number, b:number) {
  let counter = 0;
  let y = 1;
  let x = Math.pow(y,n);

  while (x > a && x < b) {
    y ++;
    counter ++;
    x = Math.pow(y,n);
    return counter
  }
}


console.log(PowerRanger(3, 1, 27))
console.log(PowerRanger(2, 49, 65))
