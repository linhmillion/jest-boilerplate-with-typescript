import {PowerRanger} from './PowerRanger';

it("should return 2 when the range is PowerRanger(2, 49, 65)", () => {
  expect(PowerRanger(2, 49, 65)).toBe(2)
})

it("expect PowerRanger(3, 1, 27)).toBe(3)", () => {
  expect(PowerRanger(3, 1, 27)).toBe(3)
})

it("should return 2 when the range is PowerRanger(2, 49, 65)", () => {
  expect(PowerRanger(10, 1, 5)).toBe(1)
})

it("should return 1 when the range is PowerRanger(5, 31, 33)", () => {
  expect(PowerRanger(5, 31, 33)).toBe(1)
})

it("should return 3 when the range is PowerRanger(4, 250, 1300)", () => {
  expect(PowerRanger(4, 250, 1300)).toBe(3)
})
