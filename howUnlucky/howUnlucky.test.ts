import { howUnlucky } from "./howUnluck";

it("shoud give 2 when it the year of 2020", () => {
  expect(howUnlucky(2020)).toBe(2)
})

it("shoud give 3 when it the year of 2026", () => {
  expect(howUnlucky(2026)).toBe(3)
})

it("shoud give 1 when it the year of 2016", () => {
  expect(howUnlucky(2016)).toBe(1)
})
