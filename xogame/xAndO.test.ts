import {XAndO} from './xAndO'
it("return an array of row and colum for winning position and false for can't win",() =>{
  expect(XAndO(["||","|X|","X||"])).toBe([1, 3])
})

it("return an array of row and colum for winning position and false for can't win",() =>{
  expect(XAndO(["X|X|O", "O|X| ", "X|O| "])).toBe([3, 3])
})

it("return false if there is no way to win", () =>{
  expect(XAndO(["X|X|O", "O|X| ", "X|O|O "])).toBe(false)})
