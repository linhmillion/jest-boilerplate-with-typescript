import { secondLargest } from "./sortNum";

it("Should return the second largest number in an array", () => {
  expect(secondLargest([10, 40, 30, 20, 50])).toBe(40);
})

it("Should return the second largest number in an array", () => {
  expect(secondLargest([25, 143, 89, 13, 105])).toBe(105);
})
it("Should return the second largest number in an array", () => {
  expect(secondLargest([54, 23, 11, 17, 10])).toBe(23);
})
it("Should return 0 when it 2 equal number in an array", () => {
  expect(secondLargest([1,1])).toBe(0);
})

it("should return the number when array have only one number", () => {
  expect(secondLargest([1])).toBe(1);
})

it("should return 0 when array empty", () => {
  expect(secondLargest([])).toBe(0);
})
