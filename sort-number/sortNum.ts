export function secondLargest (array) {
  // short array from biggest to smallest
  let sortArr = array.sort(function(a, b){return b-a});
  //check the array lengths
  let numberOfNum = sortArr.length;
  // if the array have only 1 elements or 2 element but it equal then return 0
  if ((numberOfNum === 2 && sortArr[0] === sortArr[1]) || (numberOfNum === 0) )
    return 0;
  else if (numberOfNum === 1) // only one element then return self
    return sortArr[0]
  else // return second element from sorted array which form biggest to smallest
  return sortArr[1]
}
