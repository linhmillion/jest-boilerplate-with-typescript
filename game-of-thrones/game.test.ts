import {correctTitle} from './game'

it("should return all preposition<and,of,the,in> in a lowercase string", () => {
  const text = correctTitle("jOn SnoW, kINg IN thE noRth")
  const  result = "Jon Snow, King in the North"
  expect(text).toBe(result);
} )
it("All other words should have the first character as uppercase and the rest lowercase.", () => {
  const text = correctTitle("sansa stark,lady of winterfell.")
  const  result = "Sansa Stark, Lady of Winterfell"
  expect(text).toBe(result);
})

it("All other words should have the first character as uppercase and the rest lowercase.", () => {
  const text = correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.")
  const  result = "Tyrion Lannister, Hand of the Queen."
  expect(text).toBe(result);
})

it("All titles must end with a period.", () => {
  const text = correctTitle("sansa stark,lady of winterfell.")
  const  result = "Sansa Stark, Lady of Winterfell."
  expect(text).toBe(result);
})
