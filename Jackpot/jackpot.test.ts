import { testJackpot } from './jackpot'

it("should return true if all value of are equally when they are all abc",() => {
  const result = testJackpot(["abc","abc","abc","abc"]);
  expect(result).toBe(true);
} )

it("should return true if all value of are equally when they are all @",() => {
  const result = testJackpot(["SS","SS","SS","SS"]);
  expect(result).toBe(true);
} )

it("should return false when they are not equal lengths",() => {
  const result = testJackpot(["&&","&","&&&","&&&&"]);
  expect(result).toBe(false);
} )

it("should return false when one of element capitalizes are not equal",() => {
  const result = testJackpot(["SS","SS","Ss","SS"]);
  expect(result).toBe(false);
} )
