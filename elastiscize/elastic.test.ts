import {elasticize} from './elastic';

it("should return the same string when the string is lest then 3 word", () => {
  expect(elasticize("X")).toBe("X");
})

it("should return the A DOUBLE LETTER string when the string is HAVE NUMBER OF WORD IS OOD", () => {
  expect(elasticize("ANNA")).toBe("ANNNNA");
})

it("should return the TRIPLE LETTER string when the string is HAVE NUMBER OF WORD IS EVEN NUM", () => {
  expect(elasticize("KAYAK")).toBe("KAAYYYAAK");
})
