export function elasticize(str:string) {
  let num = str.length;
  let strArray  = str.split('');
  let midString = num/2;
  let left = strArray.slice(0,midString);
  let right = strArray.slice(midString,num);
  let newLeft = left[0] + left[left.length -1] + left[left.length -1];
  let newRight = right[0] + right[0] + right[right.length -1];
  let mid = str[midString - 0.5];
  let middle = str[midString - 0.5] + str[midString - 0.5] + str[midString - 0.5]

  if (num < 3)
    return str
  else if (num % 2 == 0)
    return newLeft + newRight;
  else
    right = strArray.slice(midString+0.5,num);
    newRight =  right[0] +right[0] +right[right.length - 1]
    return newLeft + middle + newRight;
}
