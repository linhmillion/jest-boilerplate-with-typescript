// isValidIP("1.2.3.4") ➞ true
// isValidIP("1.2.3") ➞ false
//  isValidIP("1.2.3.4.5") ➞ false
//   isValidIP("123.45.67.89") ➞ true
//   isValidIP("123.456.78.90") ➞ false
//    isValidIP("123.045.067.089") ➞ false
import {isValidIP} from './ValidIP';

it("should return true if the IP is valid", () => {
  expect(isValidIP("1.2.3.4")).toBe(true)
})


it("should return true if the IP is valid", () => {
  expect(isValidIP("1.2.3")).toBe(false)
})

it("should return true if the IP is valid", () => {
  expect(isValidIP("1.2.3.4.5")).toBe(false)
})

it("should return true if the IP is valid", () => {
  expect(isValidIP("123.45.67.89")).toBe(true)
})


it("should return true if the IP is valid", () => {
  expect(isValidIP("123.456.78.90")).toBe(false)
})

it("should return true if the IP is valid", () => {
  expect(isValidIP("123.045.067.089")).toBe(false)
})
