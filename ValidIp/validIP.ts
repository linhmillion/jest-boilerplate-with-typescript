// isValidIP("1.2.3.4") ➞ true
// isValidIP("1.2.3") ➞ false
//  isValidIP("1.2.3.4.5") ➞ false
//   isValidIP("123.45.67.89") ➞ true
//   isValidIP("123.456.78.90") ➞ false
//    isValidIP("123.045.067.089") ➞ false

export function isValidIP(ip: string) {
  let arr = ip.split('.')
  let newArr = []
  arr.forEach(i => newArr.push(parseInt(i)))
  if (arr.length == 4 ) {
    if (arr[1][0] ==="0" && arr[2][0] === "0" && arr[3][0] === "0") {
      return false
    } else if(newArr[0] > 255 && newArr[1] > 255 && newArr[2] > 255 && newArr[3] > 255) {
      return false
    } else {
      return true
    }

  } else { return false}

}
